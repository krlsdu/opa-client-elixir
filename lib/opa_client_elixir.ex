defmodule OpaClient do
  @moduledoc """
  Documentation for OpaClient.
  """

  @doc """
  Hello world.

  ## Examples

      iex> OpaClient.hello()
      :world

  """
  def hello do
    :world
  end
end
