defmodule Policies do
  def list do
    "/v1/policies"
  end
  def get(id, pretty \\ "false") do
    "/v1/policies/" <> id <> "?pretty="<>pretty
  end

  def put(id, pretty \\ "false", metrics \\ "false") do
    "/v1/policies/" <> id <> "?pretty="<>pretty <> "&metrics=" <> metrics
  end

  def delete(id, pretty \\ "false", metrics \\ "false") do
    "/v1/policies/" <> id <> "?pretty="<>pretty <> "&metrics=" <> metrics
  end
end
