import Config

config :opa_client,
  opa_host: System.get_env("OPA_HOST", "http://127.0.0.1"),
  opa_port: System.get_env("OPA_PORT", "8080")


  # import_config "#{Mix.env()}.exs"
