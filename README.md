# OpaClient

Client library to consume the REST api V1 from [OPA - open policy agent](https://www.openpolicyagent.org/docs/latest/rest-api/)

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `opa_client` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:opa_client, "~> 0.1.0"},
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/opa_client](https://hexdocs.pm/opa_client).

