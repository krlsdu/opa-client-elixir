defmodule OpaClientTest do
  use ExUnit.Case
  import FakeServer

  alias FakeServer.Response

  doctest OpaClient

  test "greets the world" do
    assert OpaClient.hello() == :world
  end

  test_with_server "call resource list policies" do
    start_supervised({Finch, name: MyFinch})
    route("/v1/policies", Response.ok!("", %{"Content-Type" => "application/json"}))
    listPolicies = FakeServer.http_address() <> Policies.list()

    assert {:ok, response} = Finch.request(MyFinch, :get, listPolicies)

    assert response.status == 200
  end

  test_with_server "call a particular policy" do
    start_supervised({Finch, name: MyFinch})
    route("/v1/policies/:param", Response.ok!("", %{"Content-Type" => "application/json"}))
    getPolicy = FakeServer.http_address() <> Policies.get("example1", "true")

    assert {:ok, response} = Finch.request(MyFinch, :get, getPolicy)

    assert response.status == 200
  end

  test_with_server "create a policy" do
    start_supervised({Finch, name: MyFinch})
    route("/v1/policies/:param", Response.ok!("", %{"Content-Type" => "application/json"}))
    putPolicy = FakeServer.http_address() <> Policies.put("example1", "true", "true")

    assert {:ok, response} = Finch.request(MyFinch, :put, putPolicy)

    assert response.status == 200
  end

  test_with_server "update a policy" do
    start_supervised({Finch, name: MyFinch})
    route("/v1/policies/:param", Response.ok!("", %{"Content-Type" => "application/json"}))
    putPolicy = FakeServer.http_address() <> Policies.put("example1", "true", "true")

    assert {:ok, response} = Finch.request(MyFinch, :put, putPolicy)

    assert response.status == 200
  end

  test_with_server "delete a policy" do
    start_supervised({Finch, name: MyFinch})
    route("/v1/policies/:param", Response.ok!("", %{"Content-Type" => "application/json"}))
    deletePolicy = FakeServer.http_address() <> Policies.delete("example1", "true", "true")

    assert {:ok, response} = Finch.request(MyFinch, :delete, deletePolicy)

    assert response.status == 200
  end


end
